import random
import gradio as gr

def alternatingly_agree(message, history):
    print(history)
    if len(history) % 2 == 0:
        return f"Yes, I do think that '{message}'"
    else:
        return "I don't think so"
    


gr.ChatInterface(alternatingly_agree).launch()


# def chat_bot(input_message, history):
#     #history - лист листов [['вопрос1', 'ответ1'],['вопрос2', 'ответ2']...]
    
    
#     for message_pair in history:
#         for single_message in message_pair:
#             dialog_context.append(single_message)

#     dialog_context.append(input_message)

#     # Ограничиваем размер контекста, если он превышает максимально допустимый
#     max_dialog_context_size = 6
#     if len(dialog_context) > max_dialog_context_size:
#         dialog_context = dialog_context[-max_dialog_context_size:]
    
#     input_embedding = get_embedding(dialog_context)
#     input_embedding = torch.mean(input_embedding, dim=0).unsqueeze(0)
#     cosine_scores = cosine_similarity(input_embedding, context_embeddings)
    
#     best_match_index = cosine_scores.argmax()
#     selected_response = df.response[best_match_index]

#     dialog_context.append(selected_response)

#     return selected_response
