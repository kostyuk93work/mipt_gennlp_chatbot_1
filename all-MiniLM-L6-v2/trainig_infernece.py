import pandas as pd
import gradio as gr

import torch
from sklearn.metrics.pairwise import cosine_similarity
from sentence_transformers import SentenceTransformer

#Используем датасет с kaggle использованный для решения похожей задачи https://www.kaggle.com/code/jahysama/hey-lois-i-m-a-chatbot
data = pd.read_csv('./data/Family_guy_dialog.csv')

#Приводим к виду целевая реплика и 6 предшествующих
CHARACTER_NAME = 'Peter'

contexted = []

#context window size - 7
n_context = 7

for i in data[data['character'] == CHARACTER_NAME].index:
    if i < n_context:
        continue #remove if a context is less than 7 phrases

    prev = i - 1 - n_context
    row = []
    for j in range(i,prev,-1):
        row.append(data['dialog'][j])
    contexted.append(row)

columns = ['response', 'context']
columns = columns + ['context/' + str(i) for i in range(n_context-1)]

df = pd.DataFrame(contexted,columns=columns)


#Преобразуем датафрейм в словарь для дальнейшего использования
def get_condext_dict (data_frame):
    context_columns = ['context'] + ['context/' + str(i) for i in range(n_context-1)]
    context_dict = {}
    for index, row in data_frame[context_columns].iterrows():

        context_val = []
        for val in row.values:
            context_val.append(val)

        context_dict[index] = context_val
    return context_dict


# Используем модель all-MiniLM-L6-v2 т.к. она в общем подходит для задачи сравнения предложений и нативно умеет работать со списками эмбедингов
# Load the Sentence Transformer model
model = SentenceTransformer('all-MiniLM-L6-v2')

# Функция получается трививальной, но для совместимости с другими моделями оставляем ее
def get_embedding(sentences):
    embeddings = model.encode(sentences, convert_to_tensor=True, show_progress_bar=False)
    return embeddings


context_dict = get_condext_dict (df)

# Get embeddings for contexts
context_embeddings = []
for index, context in context_dict.items():
    context_embedding = get_embedding(context)

    # Average the embeddings of sentences in the same context
    averaged_embedding = torch.mean(context_embedding, dim=0)
    context_embeddings.append(averaged_embedding)

# Stack to create a single tensor for all context embeddings
context_embeddings = torch.stack(context_embeddings)

torch.save(context_embeddings, 'context_embeddings.pt')

def chat_bot(input_message, history):
    #history - лист листов [['вопрос1', 'ответ1'],['вопрос2', 'ответ2']...]
    
    dialog_context = []
    for message_pair in history:
        for single_message in message_pair:
            dialog_context.append(single_message)

    dialog_context.append(input_message)

    # Ограничиваем размер контекста, если он превышает максимально допустимый
    max_dialog_context_size = 6
    if len(dialog_context) > max_dialog_context_size:
        dialog_context = dialog_context[-max_dialog_context_size:]
    
    input_embedding = get_embedding(dialog_context)
    input_embedding = torch.mean(input_embedding, dim=0).unsqueeze(0)
    cosine_scores = cosine_similarity(input_embedding, context_embeddings)
    
    best_match_index = cosine_scores.argmax()
    selected_response = df.response[best_match_index]
    
    return selected_response




demo = gr.ChatInterface(
    chat_bot,
    chatbot=gr.Chatbot(height=700),
    textbox=gr.Textbox(placeholder="What are you looking at? Type!", container=False, scale=7),
    title="Peter Griffin",
    description="Hey Lois, I am a chat bot",
    theme="soft",
    examples=["Hello", "Well then, my goal becomes clear", "Damn you, woman, awake from your damnable reverie!"]
    
)

demo.launch(server_name="0.0.0.0", server_port=7010, share=True)